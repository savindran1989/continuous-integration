#include "CppUTest/TestHarness.h"
#include <string.h>
#include <stdio.h>


extern "C"
{
    #include "add.h"
}

TEST_GROUP(Module)
{
    void setup()
    {

    }

    void teardown()
    {

    }
};

TEST(Module, PositivePositiveSumTest)
{ 
    int32_t iNumber1 = 10;
    int32_t iNumber2 = 20;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(30, iSum);
}

TEST(Module, NegativeNegativeSumTest)
{ 
    int32_t iNumber1 = -10;
    int32_t iNumber2 = -20;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, NegativePositiveSumTest)
{ 
    int32_t iNumber1 = -10;
    int32_t iNumber2 = 30;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, PositiveNegativeSumTest)
{ 
    int32_t iNumber1 = 10;
    int32_t iNumber2 = -30;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, ZeroZeroSumTest)
{ 
    int32_t iNumber1 = 0;
    int32_t iNumber2 = 0;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, ZeroPositiveSumTest)
{ 
    int32_t iNumber1 = 0;
    int32_t iNumber2 = 30;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, PositiveZeroSumTest)
{ 
    int32_t iNumber1 = 40;
    int32_t iNumber2 = 0;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, NegativeZeroSumTest)
{ 
    int32_t iNumber1 = -100;
    int32_t iNumber2 = 0;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}

TEST(Module, ZeroNegativeSumTest)
{ 
    int32_t iNumber1 = 0;
    int32_t iNumber2 = -200;

    //function under test
    int32_t iSum = add_Addition(iNumber1, iNumber2);
    //check function return value
    CHECK_EQUAL(0, iSum);
}